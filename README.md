# GitLabPushTriggerer

GitLabPushTriggerer is a small application that triggers execution of scripts when given endpoints
are hit with GitLab push webhooks. It contains no permissions handling, no authentication, no
request limiting. So these must be handled in some other way, like with an Nginx reverse proxy.

Should work on any \*nix system that has `sh`.

## Installation

1. Clone repo
2. `mix deps.get`
3. Build release with `MIX_ENV=prod COOKIE=yourcookie mix release --env=prod`
4. Run release in your preferred way, setting environment according to the next section

## Environment variables

To bind to a certain port (instead of default `6000`), add env `PORT=1337`.

Configuration of endpoints and scripts is done via environment variables. Environment variables
are set in the following format:

```
GLPT_FOO='master;/path/to/bar.sh'
```

The variable consists of `GLPT_` prefix, after which is the name of the endpoint. The value is
the branch to match and path to the script to execute. The above variable would result in
an endpoint `/foo`, which would execute `/path/to/bar.sh` if a push to branch `master` was
called on the endpoint.

**NOTE:** Add the single quotes `'` to encase the value to prevent the shell messing up.

**LIMITATION:** The branch name cannot contain `;`.

The endpoint path is transformed to lowercase and underscores are replaced with dashes. See the
following examples:

* `GLPT_FOO` → `/foo`
* `GLPT_CODE_STATS_BETA` → `/code-stats-beta`
* `GLPT_AAA_` → `/aaa-`

The path to the script can be absolute or relative, as long as it can be reached from the current
working dir (for releases that can be the release dir; it is easier to use absolute paths). Scripts
are executed with `sh -c script`.

## Return value

* If the script was triggered, `200 OK` is returned.
* If the script is not found or the request path is wrong, `404 File not found` is returned.
* If the branch does not match, `400 Bad request` is returned.
* If there is an error in script execution, `500 Internal server error` is returned and the error
  is logged.
