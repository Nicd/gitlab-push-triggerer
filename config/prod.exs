use Mix.Config

# Do not print debug messages in production
config :logger, level: :error

secret = "prod.secret.exs"

if File.exists?(Path.join([__DIR__, secret])) do
  import_config(secret)
end
